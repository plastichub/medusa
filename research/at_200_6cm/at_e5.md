# Auto-Tuning - E5

## Setup

* AT (100%) started at 80Degc
* 2 brass heaters (nozzle zone) | 300W with **6 cm gap**, TC in the middle
* With screw, 40 mm barrel, 30mm screw
- no insulation
- no neighbour heating
- Control-Period : 2 secs
- SP = 202
- Start Heat : 80 DegC

## Results

compared with the 3 cm heatband layout

- holding error by 2 fold (from 1 DegC to 3 DegC) but yet, no significant overshooting
- relay oscillation by 1.5 - 2x (@todo, check & map status flags into chart)

![](./at_log_e5.JPG)

### PID Params

- Proportional Band : 9.5
- Integral Time : 356
- Derivative Time : 61
