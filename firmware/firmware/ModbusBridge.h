#ifndef MODBUS_BRIDGE_H
#define MODBUS_BRIDGE_H

#include "Addon.h"
#include <Controllino.h>

#define MAX_QUERY_BUFFER 20

// query struct
struct Query
{
    short slave;
    long addr;
    long value;
    short state;
    long fn;
};

class ModbusBridge : public Addon
{

public:
    ModbusBridge() : Addon("ModbusBridge", 50, ADDON_NORMAL)
    {
        setFlag(DEBUG);
        debug_flags = 0;
        debug_flags = 1 << DEBUG_RECEIVE;
        nextWaitingTime = 1000;
    }

    uint16_t ModbusSlaveRegisters[8];

    // Addon std implementation
    short debug(Stream *stream);
    short info(Stream *stream);
    short setup();
    short loop();
    short loop_test();

    // current query
    short id;
    short fn;
    short addr;
    int nb;

    long debug_flags;
    short queryState();
    short query(int slave, short function, long start, int coils, Addon *_addon, AddonFnPtr _mPtr);
    short qstate();

    // 0x6 callback
    AddonFnPtr updatedPtr;
    // on RawMessage
    AddonRxFn onMessage;

    // callback owner
    Addon *owner;

    int nextWaitingTime;

    enum FLAGS
    {
        DEBUG_RECEIVE = 1,
        DEBUG_SEND = 2,
    };
};

#endif