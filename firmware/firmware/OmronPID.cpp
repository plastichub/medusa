#include "OmronPID.h"
#include "ModbusBridge.h"
#include "./components/OmronE5.h"

static Query mx2Queries[MAX_QUERY_BUFFER];

void OmronPID::testPIDs()
{
    setAllSP(10);
    runAll();
    stopAll();
    singlePIDW(2, OR_E5_SWR::OR_E5_SWR_SP, 200);
    // singlePIDW(1, 5000, 20);
    // singlePID(1, ku8MBWriteSingleRegister, 0, OR_E5_CMD::OR_E5_AT_EXCECUTE);
}

short OmronPID::rawResponse(short size, uint8_t rxBuffer[])
{

    Query *current = nextQueryByState(PROCESSING);
    /*
    for (int i = 0; i < size; i++)
    {
        Serial.print(rxBuffer[i], HEX);
        Serial.print(" : ");
    }

    Serial.print("\n\t  Incoming size : ");
    Serial.print(size);
    Serial.print("\n");
    */

    if (current)
    {
        switch (current->fn)
        {
        case ku8MBWriteSingleRegister:
        {

            if (size == 5 && rxBuffer[1] == OR_E5_RESPONSE_CODE::OR_COMMAND_ERROR)
            {
                Serial.print("------ \n Command Error: ");
                Serial.print(rxBuffer[2]);
                Serial.print(" : ");
                switch (rxBuffer[2])
                {
                case OR_E5_ERROR::VARIABLE_ADDRESS_ERROR:
                {
                    Serial.println(OR_E_MSG_INVALID_ADDRESS);
                    break;
                }
                case OR_E5_ERROR::VARIABLE_RANGE_ERROR:
                {
                    Serial.println(OR_E_MSG_INVALID_RANGE);
                    break;
                }
                case OR_E5_ERROR::VARIABLE_OPERATION_ERROR:
                {
                    Serial.println(OR_E_MSG_OPERATION_ERROR);
                    break;
                }
                }
                Serial.println("\n------");
                return rxBuffer[2];
            }

            if (size == 8 && (rxBuffer[0] != current->slave || rxBuffer[2] != current->addr))
            {
                return OR_COMMAND_ERROR;
            }
            break;
        }
        }
    }
    return ERROR_OK;
}

short OmronPID::responseFn(short error)
{
    //@todo : delegate to error handler here
    OmronState &state = states[cPID];
    state.lastUpdated = millis();
    state.statusHigh = modbus->ModbusSlaveRegisters[2];
    state.statusLow = modbus->ModbusSlaveRegisters[3];
    state.pv = modbus->ModbusSlaveRegisters[1];
    state.sp = modbus->ModbusSlaveRegisters[5];
}

Query *OmronPID::nextQueryByState(uchar state = DONE)
{
    for (int i = 0; i < MAX_QUERY_BUFFER; i++)
    {
        if (queries[i].state == state)
        {
            return &queries[i];
        }
    }
    return NULL;
}

short OmronPID::queryResponse(short error)
{
    //@todo : delegate to error handler here

    Query *last = nextQueryByState(QUERY_STATE::PROCESSING);
    if (last)
    {
        last->state = QUERY_STATE::DONE;
    }
}

int OmronPID::singlePIDW(int slave, int addr, int value)
{
    singlePID(slave, ku8MBWriteSingleRegister, addr, value);
}
int OmronPID::singlePID(int slave, short fn, int addr, int value)
{
    OmronState *pid = pidBySlave(slave);
    if (pid)
    {
        Query *next = nextQueryByState(DONE);
        if (next)
        {
            next->fn = fn;
            next->slave = pid->slaveID;
            next->value = value;
            next->addr = addr;
            next->state = QUERY_STATE::QUEUED;
            return E_OK;
        }
    }
    else
    {
        Serial.println("No such PID");
        return E_NO_SUCH_PID;
    }
}
int OmronPID::eachPIDW(int addr, int value)
{
    return eachPID(ku8MBWriteSingleRegister, addr, value);
}
int OmronPID::eachPID(short fn, int addr, int value)
{
    for (short i = 0; i < NB_OMRON_PIDS; i++)
    {
        Query *next = nextQueryByState(DONE);
        if (next)
        {
            next->fn = fn;
            next->slave = states[i].slaveID;
            next->value = value;
            next->addr = addr;
            next->state = QUERY_STATE::QUEUED;
        }
        else
        {
            Serial.println("no buffer free");
        }
    }
}

OmronState *OmronPID::pidBySlave(int slave)
{
    for (short i = 0; i < NB_OMRON_PIDS; i++)
    {
        if (states[i].slaveID == slave)
        {
            return &states[i];
        }
    }
    return NULL;
}

void OmronPID::stopAll()
{
    eachPID(ku8MBWriteSingleRegister, 0, OR_E5_CMD::OR_E5_STOP);
}

void OmronPID::runAll()
{
    eachPID(ku8MBWriteSingleRegister, 0, OR_E5_CMD::OR_E5_RUN);
}

void OmronPID::setAllSP(int sp)
{
    eachPID(ku8MBWriteSingleRegister, OR_E5_SWR::OR_E5_SWR_SP, sp);
}

short OmronPID::setup()
{
    queries.setStorage(mx2Queries);
    for (uchar i = 0; i < MAX_QUERY_BUFFER; i++)
    {
        queries[i].state = QUERY_STATE::DONE;
    }
}

// for manual testing
bool did = false;

short OmronPID::loop()
{
    // return;
    if (modbus->qstate() != IDLE)
    {
        return;
    }

    if (!did)
    {
        testPIDs();
        did = true;
    }

    Query *nextCommand = nextQueryByState(QUERY_STATE::QUEUED);
    if (nextCommand)
    {
        nextCommand->state = QUERY_STATE::PROCESSING;
        modbus->nextWaitingTime = MODBUS_CMD_WAIT;
        modbus->onMessage = (AddonRxFn)&OmronPID::rawResponse;
        modbus->query(nextCommand->slave, nextCommand->fn, nextCommand->addr, nextCommand->value, this, (AddonFnPtr)&OmronPID::queryResponse);
        return;
    }

    OmronState *next = nextToUpdate();
    if (next)
    {
        modbus->nextWaitingTime = MODBUS_READ_WAIT;
        cPID = next->idx;
        modbus->query(next->slaveID, ku8MBReadHoldingRegisters, 0, 0xA, this, (AddonFnPtr)&OmronPID::responseFn);
    }
}

OmronState *OmronPID::nextToUpdate()
{
    for (short i = 0; i < NB_OMRON_PIDS; i++)
    {
        if (millis() - states[i].lastUpdated > OMRON_PID_UPDATE_INTERVAL)
        {
            return &states[i];
        }
    }
    return NULL;
}

OmronState *OmronPID::nextToWrite()
{
    for (short i = 0; i < NB_OMRON_PIDS; i++)
    {
        if (millis() - states[i].lastWritten > OMRON_PID_WRITE_INTERVAL)
        {
            return &states[i];
        }
    }
    return NULL;
}

short OmronPID::debug(Stream *stream)
{
    //*stream << this->name << ":" << this->ok();
    return false;
}
short OmronPID::info(Stream *stream)
{
    //*stream << this->name << "\n\t : " SPACE("Pin:" << MOTOR_IDLE_PIN);
    return false;
}

void OmronPID::initPIDS()
{
    for (short i = 0; i < NB_OMRON_PIDS; i++)
    {
        states[i].slaveID = slaveStart + i;
        states[i].idx = i;
    }
}
