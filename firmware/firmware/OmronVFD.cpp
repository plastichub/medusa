#include "OmronVFD.h"
#include "ModbusBridge.h"
#include "./components/OmronMX2.h"
#include "app.h"

#define valA001 3 // A001 Frequency reference source = 03 (no need to change)
#define valA002 3 // A002 Source of the “Move” command = 1 (no need to change)
#define valC026 5 // C026 Relay output function 5 (AL: error signal) = 05
#define DEF_FC_MAX_FREQ 500

void OmronVFD::doTest()
{
    Serial.println(" Do Tests ");
    pollState = false;
    //forward();
    // ping();
    run();
    //reverse();
    //run();
    // setTargetFreq(30);
    owner->timer.in(
        10000, [](OmronVFD *me) -> void {
            me->stop();
        },
        this);

    // stop();
    // configure();
}
uint16_t OmronVFD::configure()
{
    write_Single(MX2_A001, valA001);
    write_Single(MX2_A002, valA002);
    write_Single(MX2_C026, valC026); // C026 Relay output function 5 (AL: error signal) = 05
    // write_Single(MX2_A004, DEF_FC_MAX_FREQ / 10); // A004 setting the maximum frequency
    // progReg32(MX2_F002, (char *)" F002 ", FC_ACCEL_TIME);        // F002 Acceleration Time
    // progReg32(MX2_F002, (char *)" F003 ", FC_DEACCEL_TIME);      // F003 Acceleration Braking
}
uint16_t OmronVFD::updateState()
{
    readSingle_16(MX2_STATE);
    readSingle_16(MX2_TARGET_FR);
    readSingle_16(MX2_AMPERAGE);
}

////////////////////////////////////////////////////////////////////////////
//
// HMI only (Manual = A2 = 2)
uint16_t OmronVFD::stop()
{
    return write_Bit(MX2_START, 0);
}

uint16_t OmronVFD::run()
{
    return write_Bit(MX2_START, 1);
}

uint16_t OmronVFD::reverse()
{
    return write_Bit(MX2_SET_DIR, 0);
}
uint16_t OmronVFD::forward()
{
    return write_Bit(MX2_SET_DIR, 1);
}

uint16_t OmronVFD::setTargetFreq(uint16_t freq)
{
    return write_Single(MX2_TARGET_FR, freq * 100);
}

bool didTest = false;

////////////////////////////////////////////////////////////////////////////
//
// Addon impl.
short OmronVFD::setup()
{
    queries.setStorage(mx2Queries);
    for (uchar i = 0; i < MAX_QUERY_BUFFER; i++)
    {
        queries[i].state = QUERY_STATE::DONE;
    }
}

short OmronVFD::loop()
{
    if (modbus->qstate() != IDLE)
    {
        return;
    }
    if (!didTest)
    {
        didTest = true;
        doTest();
    }

    Query *nextCommand = nextQueryByState(QUERY_STATE::QUEUED);
    if (nextCommand)
    {
        nextCommand->state = QUERY_STATE::PROCESSING;
        modbus->nextWaitingTime = MODBUS_CMD_WAIT;
        modbus->onMessage = (AddonRxFn)&OmronVFD::rawResponse;
        modbus->query(nextCommand->slave, nextCommand->fn, nextCommand->addr, nextCommand->value, this, (AddonFnPtr)&OmronVFD::queryResponse);
        return;
    }

    pollState &&updateState();
}

short OmronVFD::debug(Stream *stream)
{
    //*stream << this->name << ":" << this->ok();
    return false;
}
short OmronVFD::info(Stream *stream)
{
    //*stream << this->name << "\n\t : " SPACE("Pin:" << MOTOR_IDLE_PIN);
    return false;
}

void OmronVFD::init()
{
}
