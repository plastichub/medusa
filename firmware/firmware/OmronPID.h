#ifndef OMRON_PID_H
#define OMRON_PID_H

#ifdef HAS_STATES
#include <ArduinoJson.h>
#endif

#include <Streaming.h>
#include "./Addon.h"
#include "./config.h"
#include "./common/macros.h"
#include "./components/OmronE5.h"
#include <Vector.h>

#include "ModbusBridge.h"

// actual PID, holds only values and handy functions
class OmronState
{
public:
  int statusHigh;
  int statusLow;
  int pv;
  int sp;
  int flags;
  int slaveID;
  int idx;

  millis_t lastUpdated;
  millis_t lastWritten;

  enum FLAGS
  {
    DIRTY = 1,
    UPDATED = 2
  };

  OmronState() : statusHigh(-1),
                 statusLow(-1),
                 pv(-1),
                 sp(-1),
                 flags(DIRTY),
                 lastUpdated(millis()),
                 lastWritten(millis())
  {
  }

#ifdef HAS_HMI
  bool isRunning()
  {
    return !OR_E5_STATUS_BIT(statusHigh, statusLow, OR_E5_STATUS_1::OR_E5_S1_RunStop);
  }
  bool isHeating()
  {
    return OR_E5_STATUS_BIT(statusHigh, statusLow, OR_E5_STATUS_1::OR_E5_S1_Control_OutputOpenOutput);
  }
  bool isCooling()
  {
    return OR_E5_STATUS_BIT(statusHigh, statusLow, OR_E5_STATUS_1::OR_E5_S1_Control_OutputCloseOutput);
  }
#endif
};

// Addon to deal with multiple Omron PID controllers
class OmronPID : public Addon
{
public:
  OmronPID(ModbusBridge *_bridge, short _slaveStart) : modbus(_bridge),
                                                       slaveStart(_slaveStart),
                                                       Addon(OMRON_PID_STR, OMRON_PID, ADDON_STATED)
  {
    setFlag(DEBUG);
    initPIDS();
  }

  virtual short loop();
  virtual short setup();

  short debug(Stream *stream);
  short info(Stream *stream);

  // PID access
  OmronState *OmronPID::nextToUpdate();
  OmronState *OmronPID::nextToWrite();

  // Modbus callbacks
  short responseFn(short error);
  short queryResponse(short error);

  short rawResponse(short size, uint8_t rxBuffer[]);

  // PID programming
  void stopAll();
  void runAll();
  void setAllSP(int sp);

  ///////////////////////////////////////////
  // Modbus

  Vector<Query> queries;

private:
  // config
  short slaveStart;
  short nbPIDs;

  // current PID to read updates from
  short cPID;

  ModbusBridge *modbus;

  // actual PID states
  OmronState states[NB_OMRON_PIDS];

  bool mute;

  // Modbus query / commands
  Query *nextQueryByState(uchar state = DONE);
  int eachPID(short fn, int addr, int value);
  int eachPIDW(int addr, int value);
  int singlePID(int slave, short fn, int addr, int value);
  int singlePIDW(int slave, int addr, int value);
  OmronState *pidBySlave(int slave);

protected:
  // initialize PID states
  void initPIDS();
  // for debugging and testing
  void testPIDs();
};

#endif