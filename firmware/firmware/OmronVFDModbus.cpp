#include "OmronVFD.h"
#include "ModbusBridge.h"
#include "./components/OmronMX2.h"
#include "app.h"

////////////////////////////////////////////////////////////////////////////
//
// Modbus
short OmronVFD::ping()
{
    Query *next = nextQueryByState(DONE);
    if (next)
    {
        next->fn = ku8MBLinkTestOmronMX2Only;
        next->slave = slaveAddress;
        next->value = 1234;
        next->addr = 0;
        next->state = QUERY_STATE::QUEUED;
        return E_OK;
    }
    return E_QUERY_BUFFER_END;
}
short OmronVFD::rawResponse(short size, uint8_t rxBuffer[])
{
    if (!size)
    {
        return E_OK;
    }
    Serial.print("\nIncoming:");
    Serial.print(size);

    Serial.print("::\t");
    for (int i = 0; i < size; i++)
    {
        Serial.print(rxBuffer[i], HEX);
        Serial.print(" : ");
    }

    Serial.print("\n");

    return ERROR_OK;
}

short OmronVFD::responseFn(short error)
{
}

Query *OmronVFD::nextQueryByState(uchar state = DONE)
{
    for (int i = 0; i < MAX_QUERY_BUFFER; i++)
    {
        if (queries[i].state == state)
        {
            return &queries[i];
        }
    }
    return NULL;
}

short OmronVFD::queryResponse(short error)
{
    Query *last = nextQueryByState(QUERY_STATE::PROCESSING);
    if (last)
    {
        long first = modbus->ModbusSlaveRegisters[0];
        switch (last->addr)
        {
        case MX2_STATE:
            states[0].state = first;
            break;
        case MX2_TARGET_FR:
            states[0].FC = first / 100;
            break;
        case MX2_AMPERAGE:
            states[0].current = first;
            break;
        }
        last->state = QUERY_STATE::DONE;
    }
    else
    {
        Serial.println("state error, had nothing to process");
    }
}

uint16_t OmronVFD::write_Single(uint16_t addr, unsigned int data)
{

    Query *next = nextQueryByState(DONE);
    if (next)
    {
        next->fn = ku8MBWriteSingleRegister;
        next->slave = slaveAddress;
        next->value = data;
        next->addr = addr;
        next->state = QUERY_STATE::QUEUED;
        return E_OK;
    }
    return E_QUERY_BUFFER_END;
}

uint16_t OmronVFD::write_Bit(uint16_t addr, bool on)
{
    Query *next = nextQueryByState(DONE);
    if (next)
    {
        next->fn = ku8MBWriteSingleCoil;
        next->slave = slaveAddress;
        next->addr = addr - 1;
        modbus->ModbusSlaveRegisters[0] = on;
        next->state = QUERY_STATE::QUEUED;
        return E_OK;
    }
    return E_QUERY_BUFFER_END;
}

short OmronVFD::readSingle_16(int addr)
{
    Query *next = nextQueryByState(DONE);
    if (next)
    {
        next->fn = ku8MBReadHoldingRegisters;
        next->slave = slaveAddress;
        next->value = 1;
        next->addr = addr - 1;
        next->state = QUERY_STATE::QUEUED;
        return E_OK;
    }
    return E_QUERY_BUFFER_END;
}
