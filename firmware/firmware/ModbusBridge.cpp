#include <Streaming.h>
#include "./Addon.h"
#include "ModbusRtu.h"

#include "ModbusBridge.h"
#include "enums.h"

#define RS485Serial 3

#define MasterModbusAdd 0
#define SlaveModbusAdd 1

Modbus master(MasterModbusAdd, RS485Serial);

modbus_t ModbusQuery[1];
uint16_t ModbusSlaveRegisters[8];
millis_t WaitingTime;

int _state = IDLE;

short ModbusBridge::setup()
{
   master.begin(19200, SERIAL_8E1); // SERIAL_8E1
   master.setTimeOut(2000);
   WaitingTime = millis() + nextWaitingTime;
   _state = IDLE;
}

short ModbusBridge::qstate()
{
   return _state;
}

short ModbusBridge::loop()
{
   loop_test();
}

short ModbusBridge::query(int slave, short function, long start, int coils, Addon *_addon, AddonFnPtr _mPtr)
{
   if (_state != IDLE)
   {
      return WAITING;
   }

   nb = 0;
   addr = 0;
   id = slave;
   fn = function;
   addr = start;
   nb = coils;
   owner = _addon;
   updatedPtr = _mPtr;
   _state = WAITING;

   return ERROR_OK;
}

short ModbusBridge::loop_test()
{
   switch (_state)
   {

   case IDLE:
   {
      return;
   }

   case WAITING:
   {
      if (millis() > WaitingTime)
      {
         _state++; // wait state
      }
      break;
   }
   case QUERY:
   {
      ModbusQuery[0].u8id = id;                      // slave address
      ModbusQuery[0].u8fct = fn;                     // function code (this one is registers read)
      ModbusQuery[0].u16RegAdd = addr;               // start address in slave
      ModbusQuery[0].u16CoilsNo = nb;                // number of elements (coils or registers) to read
      ModbusQuery[0].au16reg = ModbusSlaveRegisters; // pointer to a memory array in the CONTROLLINO

      master.query(ModbusQuery[0]); // send query (only once)
      _state++;
      break;
   }
   case RESPONSE:
   {
      master.poll(); // check incoming messages
      if (master.getState() == COM_IDLE)
      {
         long onMessageError = 0;
         if (owner && onMessage)
         {
            onMessageError = (owner->*onMessage)(master.rxSize, master.rxBuffer);
         }

         _state = IDLE;
         short ret = (owner->*updatedPtr)(onMessageError);
         WaitingTime = millis() + nextWaitingTime;

         if (TEST(debug_flags, DEBUG_RECEIVE) && onMessageError == ERROR_OK)
         {
            // registers read was proceed
            // Serial.println("---------- READ RESPONSE RECEIVED ----");
            // Serial.println(nb);
            /*
            for (int i = 0; i < nb; i++)
            {
               Serial.print("\t REG-");
               Serial.print(i);
               Serial.print(": ");
               Serial.print(ModbusSlaveRegisters[i], DEC);
            }
            */
            /*
            Serial.print(" REG-0: ");
            Serial.print(ModbusSlaveRegisters[0], DEC);
            Serial.print(" REG-1: ");
            Serial.print(ModbusSlaveRegisters[1], DEC);
            Serial.print(" REG-2: ");
            Serial.print(ModbusSlaveRegisters[2], DEC);
            Serial.print(" REG-3: ");
            Serial.print(ModbusSlaveRegisters[3], DEC);
            Serial.print(" REG-4: ");
            Serial.print(ModbusSlaveRegisters[4], DEC);
            Serial.print(" REG-5: ");
            Serial.print(ModbusSlaveRegisters[5], DEC);
            Serial.print(" REG-6: ");
            Serial.print(ModbusSlaveRegisters[6], DEC);
            Serial.print(" REG-7: ");
            Serial.print(ModbusSlaveRegisters[7], DEC);
            Serial.print(" REG-8: ");
            Serial.print(ModbusSlaveRegisters[8], DEC);
            Serial.print(" REG-9: ");
            Serial.println(ModbusSlaveRegisters[9], DEC);
            */
            Serial.println("");
         }
      }
      break;
   }
   }
}

short ModbusBridge::debug(Stream *stream)
{
   // *stream << this->name << ":";
   return false;
}

short ModbusBridge::info(Stream *stream)
{
   // *stream << this->name << "\n\t";
}