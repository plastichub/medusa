### Omron MX2

1. Please refer to the [OmronMX2 - Modbus setup guide](../vendor/omron/P641-E1-01_EGuide_CJ_Mod485_OMRON_3G3MX2-V1.pdf - Section 7.2.2)

In case the terminal labels mismatch the documenation, please use `SN` for (A-) and `SP` for (B+)

2. The firmware expects the VFD at **Slave-Address 10** !

Additionally, please check the [user manual](../vendor/omron/I570-E2-02B.pdf)

### Omron E5 - PID
